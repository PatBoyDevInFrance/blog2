<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(ArticleRepository $repoArticle, CategoryRepository $repoCategory ): Response
    {   
        $catego = $repoCategory->findAll();  
        $articles = $repoArticle->findAll();
        return $this->render('home/index.html.twig', [
            'articles' => $articles,
            'categories' => $catego
        ]);
    }

    #[Route('/show/{id}', name: 'show')]
    public function show($id,Article $article): Response
    {   
        
        if(!$article){
            return $this->redirectToRoute('home');

        }
        //dd($id);
        return $this->render('show/index.html.twig',[
            'article' => $article
        ]);
    }
    #[Route('/showArticle/{id}', name: 'show_article')]
    public function showArticle(Category $category): Response
    {   
        
        $articles = $category->getArticles();
        
        //dd($id);
        return $this->render('show/showArticle.html.twig',[
           
        ]);
    }
}
